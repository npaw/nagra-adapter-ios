//
//  YBNagraAdapterSwiftTranformer.swift
//  YouboraNagraAdapter
//
//  Created by Elisabet Massó on 03/12/2021.
//  Copyright © 2021 NPAW. All rights reserved.
//

import Foundation
import YouboraAVPlayerAdapter

public class YBNagraAdapterSwiftTranformer: YBAVPlayerAdapterSwiftTranformer {
    
    class func transform(from adapter: YBNagraPlayerAdapter) -> YBPlayerAdapter<AnyObject>? {
        return super.transform(from: adapter)
    }
    
}
