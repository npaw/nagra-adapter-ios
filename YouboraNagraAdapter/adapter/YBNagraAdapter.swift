//
//  YBNagraAdapter.swift
//  YouboraNagraAdapter
//
//  Created by Elisabet Massó on 03/12/2021.
//  Copyright © 2021 NPAW. All rights reserved.
//

import Foundation
import OPYSDKFPS
import YouboraLib
import YouboraAVPlayerAdapter

public class YBNagraPlayerAdapter: YBAVPlayerAdapter {
    
    private var lastBirateIndex = -1
    private var lastBirate = 0.0

    private override init() {
        super.init()
    }
    
    public init(player: OTVAVPlayer) {
        super.init(player: player)
    }
    
    public override func registerListeners() {
        super.registerListeners()
        
        NotificationCenter.default.addObserver(self, selector: #selector(handleNetworkAnalyticsNotification(notificaition:)), name: .OTVNetworkAnalyticsNotification, object: nil)
    }
    
    public override func getBitrate() -> NSNumber? {
        guard let player = player as? OTVAVPlayer, let bitrate = player.networkAnalytics?.adaptiveStreaming.selectedBitrate() else { return nil }
        lastBirate = bitrate
        return NSNumber(value: bitrate)
        
    }
    
    public override func getRendition() -> String? {
        guard let player = player as? OTVAVPlayer else { return nil }
        
        var bitrateIndex = -1
        var bitrate = 0.0
        
        guard let availableBitrates = player.networkAnalytics?.adaptiveStreaming.availableBitrates() else { return nil }
        
        for (index, bitrateValue) in availableBitrates.enumerated() {
            if Int(lastBirate) == bitrateValue {
                bitrate = lastBirate
                bitrateIndex = index
            }
        }

        if let resolution = player.currentItem?.presentationSize, bitrateIndex != lastBirateIndex {
            lastBirateIndex = bitrateIndex
            print("REL: \(YBYouboraUtils.buildRenditionString(withWidth: Int32(resolution.width), height: Int32(resolution.height), andBitrate: bitrate))")
            return YBYouboraUtils.buildRenditionString(withWidth: Int32(resolution.width), height: Int32(resolution.height), andBitrate: bitrate)
        }
        
        return nil
    }
    
    public override func fireStop() {
        lastBirateIndex = -1
        lastBirate = 0.0
        super.fireStop()
    }
    
    @objc func handleNetworkAnalyticsNotification(notificaition: NSNotification) {
        guard let player = player as? OTVAVPlayer, let otvNetworkNotificationType = notificaition.object as? OTVNetworkAnalytics.Event else { return }
        
        if otvNetworkNotificationType == .errorChanged && player.networkAnalytics?.httpErrorMessage != "" && player.networkAnalytics?.httpError != -1001 {
            fireError(withMessage: player.networkAnalytics?.httpErrorMessage, code: "\(player.networkAnalytics?.httpError ?? 0)", andMetadata: nil)
        }
    }
    
}
