//
//  AppDelegate.swift
//  NagraSample
//
//  Created by Elisabet Massó on 03/12/2021.
//  Copyright © 2021 NPAW. All rights reserved.
//

import UIKit

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?

    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        // Override point for customization after application launch.
        window = UIWindow(frame: UIScreen.main.bounds)
        
        let rootViewController = UINavigationController()
        
        if let menuViewController = MenuViewController.initFromXIB() {
            rootViewController.viewControllers = [menuViewController]
        }
        
        window?.rootViewController = rootViewController
        
        
        window?.makeKeyAndVisible()
        return true
    }

}

