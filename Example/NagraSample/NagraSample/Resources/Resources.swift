//
//  Resources.swift
//  NagraSample
//
//  Created by Elisabet Massó on 03/12/2021.
//  Copyright © 2021 NPAW. All rights reserved.
//

import Foundation

struct Resource {
    static let a100004356 = "http://live.vtv-preprod.vodafone.de/100004346/vxfmt=dp/manifest.mpd?device_profile=DASH_STB_NGRSSP_LIVE_HD"
    static let b100004356 = "http://live.vtv-preprod.vodafone.de/100004346/vxfmt=hls/playlist.m3u8"
    
    static let a100005156 = "http://live.vtv-preprod.vodafone.de/100005156/vxfmt=dp/manifest.mpd?device_profile=DASH_STB_NGRSSP_LIVE_HD"
    static let b100005156 = "http://live.vtv-preprod.vodafone.de/100005156/vxfmt=hls/playlist.m3u8device_profile=HLS_MOBILE_NGRSSP_LIVE_HD"
    
    static let a100005169 = "http://live.vtv-preprod.vodafone.de/100005169/vxfmt=dp/manifest.mpd?device_profile=DASH_STB_NGRSSP_LIVE_HD"
    static let b100005169 = "http://live.vtv-preprod.vodafone.de/100005169/vxfmt=hls/playlist.m3u8device_profile=HLS_MOBILE_NGRSSP_LIVE_HD"
    
    static let a100004346 = "http://live.vtv-preprod.vodafone.de/100004346/vxfmt=dp/manifest.mpd?device_profile=DASH_STB_NGRSSP_LIVE_HD"
    static let b100004346 = "http://live.vtv-preprod.vodafone.de/100004346/vxfmt=hls/playlist.m3u8device_profile=HLS_MOBILE_NGRSSP_LIVE_HD"
    
    static let a100005141 = "http://live.vtv-preprod.vodafone.de/100005141/vxfmt=dp/manifest.mpd?device_profile=DASH_STB_NGRSSP_LIVE_HD"
    static let b100005141 = "http://live.vtv-preprod.vodafone.de/100005141/vxfmt=hls/playlist.m3u8device_profile=HLS_MOBILE_NGRSSP_LIVE_HD"
    
    static let a100004993 = "http://live.vtv-preprod.vodafone.de/100004993/vxfmt=dp/manifest.mpd?device_profile=DASH_STB_NGRSSP_LIVE_HD"
    static let b100004993 = "http://live.vtv-preprod.vodafone.de/100004993/vxfmt=hls/playlist.m3u8device_profile=HLS_MOBILE_NGRSSP_LIVE_HD"
    
    static let a100010360 = "http://live.vtv-preprod.vodafone.de/100010360/vxfmt=dp/manifest.mpd?device_profile=DASH_STB_NGRSSP_LIVE_HD"
    static let b100010360 = "http://live.vtv-preprod.vodafone.de/100010360/vxfmt=hls/playlist.m3u8device_profile=HLS_MOBILE_NGRSSP_LIVE_HD"
    
    static let a100010590 = "http://live.vtv-preprod.vodafone.de/100010590/vxfmt=dp/manifest.mpd?device_profile=DASH_STB_NGRSSP_LIVE_HD"
    static let b100010590 = "http://live.vtv-preprod.vodafone.de/100010590/vxfmt=hls/playlist.m3u8device_profile=HLS_MOBILE_NGRSSP_LIVE_HD"

}

struct AdResource {
    static let bitdash = "https://bitdash-a.akamaihd.net/content/MI201109210084_1/m3u8s/f08e80da-bf1d-4e3d-8899-f0f6155f6efa.m3u8"
}
