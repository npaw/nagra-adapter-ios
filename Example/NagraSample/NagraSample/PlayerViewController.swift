//
//  PlayerViewController.swift
//  NagraSample
//
//  Created by Elisabet Massó on 03/12/2021.
//  Copyright © 2021 NPAW. All rights reserved.
//

import UIKit
import YouboraLib
import YouboraConfigUtils
import YouboraNagraAdapter
import OPYSDKFPS

class PlayerView: UIView {
    
    var player: AVPlayer? {
        get { return playerLayer.player }
        set { playerLayer.player = newValue }
    }
    
    var playerLayer: AVPlayerLayer {
        return layer as! AVPlayerLayer
    }
    
    override class var layerClass: AnyClass {
        return AVPlayerLayer.self
    }
}

class PlayerViewController: UIViewController {
    
    var plugin: YBPlugin?
    
    var resource: String?
    
    var player: OTVAVPlayer?
    
    var timer: Timer?
    
    var isTouchingSlider = false
    
    var changeItemButton = UIButton(type: .custom)
    var buttonReplay = UIButton(type: .custom)
    var playerContainer = PlayerView()
    
    private let controlBtn: UIButton = {
        let button = UIButton(type: .system)
        let image = UIImage(named: "pause")
        button.setImage(image, for: .normal)
        button.tintColor = UIColor.white
        button.translatesAutoresizingMaskIntoConstraints = false
        button.addTarget(self, action: #selector(handleControls), for: .touchUpInside)
        button.isHidden = false
        return button
    }()
    
    private let leftTimeLbl: UILabel = {
        let label = UILabel()
        label.text = "00:00"
        label.textAlignment = .right
        label.textColor = UIColor.white
        label.font = UIFont.boldSystemFont(ofSize: 14)
        label.translatesAutoresizingMaskIntoConstraints = false
        label.isHidden = true
        return label
    }()
    
    private let currentTimeLbl: UILabel = {
        let label = UILabel()
        label.text = "00:00"
        label.textAlignment = .left
        label.textColor = UIColor.white
        label.font = UIFont.boldSystemFont(ofSize: 14)
        label.translatesAutoresizingMaskIntoConstraints = false
        label.isHidden = true
        return label
    }()
    
    private let videoSlider: UISlider = {
        let slider = UISlider()
        slider.minimumTrackTintColor = UIColor.red
        slider.maximumTrackTintColor = UIColor.white
        slider.thumbTintColor = UIColor.red
        slider.translatesAutoresizingMaskIntoConstraints = false
        slider.addTarget(self, action: #selector(handleSliderValueChanged), for: [.touchUpInside, .touchDown])
        slider.isHidden = true
        return slider
    }()
    
    private let container: UIView = {
        let view = UIView()
        view.translatesAutoresizingMaskIntoConstraints = false
        return view
    }()
    
    private let indicator: UIActivityIndicatorView = {
        let uiIndicator = UIActivityIndicatorView()
        uiIndicator.hidesWhenStopped = true
        uiIndicator.color = .white
        return uiIndicator
    }()

    override func viewDidLoad() {
        super.viewDidLoad()

        view.backgroundColor = .black
        
        let options = YouboraConfigManager.getOptions()
        options.contentIsLive = NSNumber(false)
        plugin = YBPlugin(options: options)
        
        // Initialize player on this view controller
        initializePlayer()
    }
    
    override func viewWillDisappear(_ animated: Bool) {
        super.viewWillDisappear(animated)
         
        if self.isMovingFromParent {
          timer?.invalidate()
          timer = nil
          plugin?.fireStop()
          plugin?.removeAdapter()
        }
         
      }
       
      private func configureControls() {
        view.backgroundColor = .black
         
        changeItemButton.setTitle("Change Item", for: .normal)
        changeItemButton.addTarget(self, action: #selector(changeItem), for:.touchUpInside)
        changeItemButton.translatesAutoresizingMaskIntoConstraints = false
        view.addSubview(changeItemButton)
        NSLayoutConstraint.activate([
          changeItemButton.topAnchor.constraint(equalTo: view.topAnchor, constant: 100),
          changeItemButton.leftAnchor.constraint(equalTo: view.leftAnchor, constant: 0),
          changeItemButton.rightAnchor.constraint(equalTo: view.rightAnchor, constant: 0),
          changeItemButton.heightAnchor.constraint(equalToConstant: 20)
        ])
         
        buttonReplay.setTitle("Replay", for: .normal)
        buttonReplay.addTarget(self, action: #selector(pressToReplay), for:.touchUpInside)
        buttonReplay.translatesAutoresizingMaskIntoConstraints = false
        view.addSubview(buttonReplay)
        NSLayoutConstraint.activate([
          buttonReplay.topAnchor.constraint(equalTo: changeItemButton.bottomAnchor, constant: 20),
          buttonReplay.leftAnchor.constraint(equalTo: view.leftAnchor, constant: 0),
          buttonReplay.rightAnchor.constraint(equalTo: view.rightAnchor, constant: 0),
          buttonReplay.heightAnchor.constraint(equalToConstant: 20)
        ])
        buttonReplay.isHidden = true
         
        playerContainer.translatesAutoresizingMaskIntoConstraints = false
        view.addSubview(playerContainer)
        NSLayoutConstraint.activate([
          playerContainer.topAnchor.constraint(equalTo: buttonReplay.bottomAnchor, constant: 0),
          playerContainer.leftAnchor.constraint(equalTo: view.leftAnchor, constant: 0),
          playerContainer.rightAnchor.constraint(equalTo: view.rightAnchor, constant: 0),
          playerContainer.bottomAnchor.constraint(equalTo: view.bottomAnchor, constant: 0)
        ])

        container.frame = view.bounds
        view.addSubview(container)
        container.topAnchor.constraint(equalTo: playerContainer.topAnchor, constant: 0).isActive = true
        container.rightAnchor.constraint(equalTo: view.rightAnchor).isActive = true
        container.leftAnchor.constraint(equalTo: view.leftAnchor).isActive = true
        container.heightAnchor.constraint(equalTo: view.heightAnchor, multiplier: 9/16).isActive = true
         
        container.addSubview(indicator)
        container.bringSubviewToFront(indicator)
        indicator.center = container.center
         
        controlBtn.frame = container.bounds
        controlBtn.bounds = controlBtn.frame.insetBy(dx: 20.0, dy: 20.0)
        container.addSubview(controlBtn)
        controlBtn.leftAnchor.constraint(equalTo: container.leftAnchor, constant: 10).isActive = true
        controlBtn.bottomAnchor.constraint(equalTo: container.bottomAnchor, constant: -8).isActive = true
        controlBtn.widthAnchor.constraint(equalToConstant: 15).isActive = true
        controlBtn.heightAnchor.constraint(equalToConstant: 15).isActive = true
         
    //    if let contentIsLive = plugin?.options.contentIsLive, contentIsLive.isEqual(to: NSNumber(booleanLiteral: false)) {
          buttonReplay.isHidden = false
          leftTimeLbl.frame = container.bounds
          container.addSubview(leftTimeLbl)
          leftTimeLbl.rightAnchor.constraint(equalTo: container.rightAnchor, constant: -10).isActive = true
          leftTimeLbl.bottomAnchor.constraint(equalTo: container.bottomAnchor).isActive = true
          leftTimeLbl.widthAnchor.constraint(equalToConstant: 60).isActive = true
          leftTimeLbl.heightAnchor.constraint(equalToConstant: 30).isActive = true
          leftTimeLbl.isHidden = false

          currentTimeLbl.frame = container.bounds
          container.addSubview(currentTimeLbl)
          currentTimeLbl.leftAnchor.constraint(equalTo: controlBtn.rightAnchor, constant: 10).isActive = true
          currentTimeLbl.bottomAnchor.constraint(equalTo: container.bottomAnchor).isActive = true
          currentTimeLbl.widthAnchor.constraint(equalToConstant: 60).isActive = true
          currentTimeLbl.heightAnchor.constraint(equalToConstant: 30).isActive = true
          currentTimeLbl.isHidden = false

          videoSlider.frame = container.bounds
          container.addSubview(videoSlider)
          videoSlider.rightAnchor.constraint(equalTo: leftTimeLbl.leftAnchor).isActive = true
          videoSlider.bottomAnchor.constraint(equalTo: container.bottomAnchor).isActive = true
          videoSlider.leftAnchor.constraint(equalTo: currentTimeLbl.rightAnchor).isActive = true
          videoSlider.heightAnchor.constraint(equalToConstant: 30).isActive = true
          videoSlider.isHidden = false
    //    }
         
        view.layoutIfNeeded()
      }
    
    @objc
    private func handleControls() {
        guard let player = playerContainer.player else { return }
        if player.timeControlStatus == .paused {
            player.play()
            controlBtn.setImage(UIImage(named: "pause"), for: .normal)
        } else if player.timeControlStatus == .playing {
            player.pause()
            controlBtn.setImage(UIImage(named: "play"), for: .normal)
        } else {
            pressToReplay()
            controlBtn.setImage(UIImage(named: "pause"), for: .normal)
        }
    }
    
    @objc
    private func handleSliderValueChanged() {
        if let duration = playerContainer.player?.currentItem?.duration {
            let value = Double(videoSlider.value) * CMTimeGetSeconds(duration)
            playerContainer.player?.seek(to: CMTimeMakeWithSeconds(value, preferredTimescale: 1))
            isTouchingSlider = !isTouchingSlider
        }
    }
    
    @objc
    func handleTime() {
        if let currentTime = playerContainer.player?.currentItem?.currentTime(), let duration = playerContainer.player?.currentItem?.duration {
            currentTimeLbl.text = "\(getTextTimeFrom(time: CMTimeGetSeconds(currentTime)))"
            leftTimeLbl.text = "-\(getTextTimeFrom(time: CMTimeGetSeconds(duration - currentTime)))"
            if !isTouchingSlider {
                videoSlider.value = Float(CMTimeGetSeconds(currentTime) / CMTimeGetSeconds(duration))
            }
        }
    }
    
    @objc
    func changeItem() {
        guard let newResource = ["https://d3bqrzf9w11pn3.cloudfront.net/basic_hls_bbb_clear/index.m3u8"].randomElement(), let url = URL(string: newResource) else {
            return
        }
        resource = newResource
        plugin?.options.contentIsLive = NSNumber(true)
        
        // Change content in player
        playerContainer.player?.replaceCurrentItem(with: OTVAVPlayerItem(url: url))
        
        configureControls()
    }
    
    @objc
    func pressToReplay() {
        playerContainer.player?.seek(to: .zero)
        playerContainer.player?.play()
        configureControls()
    }
    
    private func getTextTimeFrom(time totalSeconds: Double) -> String {
        if !totalSeconds.isNaN && !totalSeconds.isInfinite {
            let seconds = Int(totalSeconds.truncatingRemainder(dividingBy: 60))
            let secondsText = String(format: "%02d", seconds)
            let minutesText = String(format: "%02d", Int(totalSeconds / 60))
            return "\(minutesText):\(secondsText)"
        }
        return ""
    }
    
}

extension PlayerViewController {
    
    private func initializePlayer() {
        
        guard let resource = resource, let url = URL(string: resource) else { return }
        
        // Create player
        let player = OTVAVPlayer(url: url)
        playerContainer.player = player
        
        plugin?.adapter = YBNagraAdapterSwiftTranformer.transform(from: YBNagraPlayerAdapter(player: player))
        
        configureControls()
        
        // Start playback
        player.play()
        
        if timer == nil {
            timer = Timer.scheduledTimer(timeInterval: 0.05, target: self, selector: #selector(handleTime), userInfo: nil, repeats: true)
        }
        
        handleControls()
    }
    
}
