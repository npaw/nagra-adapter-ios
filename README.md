# YouboraNagraAdapter #

A framework that will collect several video events from the Nagra player and send it to the back end

# Installation #

#### Cloning the repo ####

In shell located in your project folder run

```bash
git clone https://bitbucket.org/npaw/nagra-adapter-ios.git
```

add the target in your Podfile

```bash
target 'YouboraNagraAdapter' do
	project 'nagra-adapter-ios/YouboraNagraAdapter.xcodeproj'
	use_frameworks!
	# Pods for YouboraNagraAdapter
	pod 'YouboraLib', '~> 6.5'
end
```

then run

```bash
pod install
```

and then:

* Select the project file in the "Project" navigator column, and the target in the project and targets list
* In the "General" tab scroll down to the "Linked Frameworks and Libraries" section and click the "+" button
* Now look for the YouboraNagraAdapter.framework in the options, select it and press "Add"

## How to use ##

## Start plugin and options ##

```swift

//Import
import YouboraLib

...

//Config Options and init plugin (do it just once for each play session)

var options: YBOptions {
    let options = YBOptions()
    options.contentResource = "http://example.com"
    options.accountCode = "accountCode"
    options.adResource = "http://example.com"
    options.contentIsLive = NSNumber(value: false)
    return options;
}
    
lazy var plugin = YBPlugin(options: self.options)
```

### YBNagraAdapter ###

```swift
import YouboraNagraAdapter

...

// Once you have your player and plugin initialized you can set the adapter
plugin.adapter = YBNagraAdapterSwiftTranformer.transform(from: YBNagraPlayerAdapter(player: player))

...

// When the view gonna be destroyed you can force stop and clean the adapters in order to make sure you avoid retain cycles  
plugin.fireStop()
plugin.removeAdapter()
```

## Run samples project ##

###### Via the cloned repo ######

Once the repo is cloned open the ```nagra-adapter-ios``` folder, import the missing dependencies and run

```bash
pod install
```

The project should be ready now to work

